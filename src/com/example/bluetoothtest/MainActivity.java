package com.example.bluetoothtest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;

import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.speech.tts.TextToSpeech;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity
	implements AdapterView.OnItemClickListener {
	
    private final static String BR = System.getProperty("line.separator");
	private final static int MP = LinearLayout.LayoutParams.MATCH_PARENT;
	private final static int WC = LinearLayout.LayoutParams.WRAP_CONTENT;

	private static final int REQUEST_ENABLE_BLUETOOTH = 2;
	
	private BluetoothAdapter     btAdapter;
	private ArrayAdapter<String> adapter; //リストビューのアダプター
	private TextToSpeech tts;     //TTS
	private String path = "/sdcard/bluetooth_tmp/members.txt";
	
	// Bluetooth接続パラメータ格納用HashMapオブジェクト
	private HashMap<String, Integer> bluetoothParamArray = new HashMap<String, Integer>();

		//String name =  "mika";
		int value =  0;
		
	String path0 =  Environment.getExternalStorageDirectory().getAbsolutePath() + "/bluetooth_tmp/";
 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//自動生成されたR.javaの定数を指定してXMLからレイアウトを生成
		setContentView(R.layout.activity_main);	
		
		File file0 = new File(path0); 
		
		boolean isExists0 = file0.exists();
        if(isExists0==false){
        	file0.mkdirs();
        }
        	
        File file = new File(path);  
	    boolean isExists = file.exists();


	    if(isExists==false){
 	        	try{
 	        	file.createNewFile();
 	        	}catch(IOException e){
 	        	}

 	      }
        else{
        	
        }
		
		//BluetoothAdapter取得
		btAdapter = BluetoothAdapter.getDefaultAdapter();
		
		//WiFi
		WifiManager manager = (WifiManager)getSystemService(WIFI_SERVICE);
		if(manager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
			//APをスキャン
			manager.startScan();

		}

		if(!btAdapter.equals(null)){
		}else{
			Toast.makeText(this, "このデバイスはBluetoothをサポートしていません",Toast.LENGTH_LONG).show();
			finish();
		}
		
		//レイアウトの生成
		LinearLayout layout = new LinearLayout(this);
		layout.setOrientation(LinearLayout.VERTICAL);
		setContentView(layout);

 

		//リストビューのアダプタの生成
		adapter = new ArrayAdapter<String>(this, R.layout.list_layout);
		
		//リストビューの生成
		ListView listView = new ListView(this);
		listView.setLayoutParams(new LinearLayout.LayoutParams(MP, WC));
		listView.setAdapter(adapter);
		layout.addView(listView);
		//listView.setOnItemClickListener(this);
		
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
			ListView listView = (ListView) parent;
			// クリックされたアイテムを取得します
			String item = (String) listView.getItemAtPosition(position);
				File file2 = new File(path);
				try {
					BufferedReader br = new BufferedReader(new FileReader(file2));

					String str;
						str = br.readLine();
					while(str != null){
						str = br.readLine();
						if(item.matches(".*" + str + ".*")){
							Toast.makeText(MainActivity.this,"会ったことがあります" ,Toast.LENGTH_LONG).show();	
							break;
						}
						else{
							continue;
						}
					}	
					try {
						File file = new File(path);
						FileWriter filewriter = new FileWriter(file, true);
						filewriter.write(item+"\n");
						filewriter.close();
				    } catch (FileNotFoundException e) {
				    } catch (IOException e) {
				    }
					br.close();	
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		});
		
		//ブロードキャストレシーバの追加
		IntentFilter filter;
		filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		registerReceiver(receiver, filter);
		filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		registerReceiver(receiver, filter);
		
		
		//Bluetoothの設定がONかどうかを確認する
		boolean btEnable = btAdapter.isEnabled();
		
		if(btEnable == true){
			//BluetoothがONだった場合の処理
			
			///////////////////////////////////////////////////////////////////////////
			Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
			if (pairedDevices.size()>0){
				for (BluetoothDevice device:pairedDevices){
					adapter.add(device.getName());
					//adapter.add(device.getName());
				}
			}
			if (btAdapter.isDiscovering()) btAdapter.cancelDiscovery();
			btAdapter.startDiscovery();			
            ///////////////////////////////////////////////////////////////////////////
	
		}else{
			//BluetoothがOFFだった場合の処理
			Intent bluetoothOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(bluetoothOn, REQUEST_ENABLE_BLUETOOTH);
		}
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int ResultCode, Intent date){
		if(requestCode == REQUEST_ENABLE_BLUETOOTH){
			if(ResultCode == Activity.RESULT_OK){
				Toast.makeText(this, "BluetoothをONにしました",Toast.LENGTH_LONG).show();
                ///////////////////////////////////////////////////////////////////////////
				Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
				if (pairedDevices.size()>0){
					for (BluetoothDevice device:pairedDevices){
						adapter.add(device.getName());
					}
				}
				if (btAdapter.isDiscovering()) btAdapter.cancelDiscovery();
				btAdapter.startDiscovery();
                ///////////////////////////////////////////////////////////////////////////				
			}else{
				finish();
			}
		}
			
			
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bluetooth, menu);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		// Bluetooth端末の検索のキャンセル
		btAdapter.cancelDiscovery();
		
	}
	
	public final BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent){
			String action = intent.getAction();
			
			//Bluetooth 発見
			if (BluetoothDevice.ACTION_FOUND.equals(action)){
				BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				if(device.getBondState()!=BluetoothDevice.BOND_BONDED){
					if(bluetoothParamArray.containsKey(device.getName())){
						value = bluetoothParamArray.get(device.getName());
						value++;
						bluetoothParamArray.put(device.getName(), value);
					}
					else{
					//getName()・・・デバイス名取得メソッド
					//getAddress()・・・デバイスのMACアドレス取得メソッド
					adapter.add(device.getName());
					bluetoothParamArray.put(device.getName(), value);
					
					}
					
				
				}else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
					
				}
			
			}else{
				
			}
			
		}
	
};
}



	